This is a simple C# app which helps interface the Tektronix TDS 340 100MHz digital storage oscilloscope with a Windows computer using the RS-232 port to retrieve raw signal data.

For details, see my blog post at http://www.toughdev.com/content/2014/01/programming-the-tektronix-tds-340-100mhz-digital-storage-oscilloscope/